#include <stdio.h>
#include <math.h>

__global__ void calcFluxes(const float *us, float *fs, const float a,
        const int N)
{
    int tid = threadIdx.x + blockIdx.x * blockDim.x;

    if (tid > 0 && tid <= N) {
        fs[tid] = a * us[tid - 1];
    }
    else if (tid == 0) {
        fs[tid] = 0.0;
    }
    else if (tid == N+1) {
        fs[tid] = a * us[tid - 1];
    }
    return;
}

__global__ void calcValues(float *us, const float *fs, const float dtondx,
        const int N)
{
    int tid = threadIdx.x + blockIdx.x * blockDim.x;

    if (tid < N) {
        us[tid] = us[tid] - dtondx * (fs[tid + 1] - fs[tid]);
    }
    return;
}

float initialProfile(const float x)
{
    if (x >= 2.0 and x <= 3.0) {
        return sin(2.0 * M_PI * x);
    }
    else {
        return 0.0;
    }
}

int main()
{
    const int N = 100;
    const float L = 5.0;
    const float dx = L / N;

    /// declare host array variables
    float *xs;
    float *us;
    float *fs;

    /// allocate host-side data
    xs = (float *)malloc(    N * sizeof(float));
    us = (float *)malloc(    N * sizeof(float));
    fs = (float *)malloc((N+1) * sizeof(float));

    /// declare device array variables
    float *d_us;
    float *d_fs;

    /// allocate device-side data
    cudaMalloc(&d_us,     N * sizeof(float));
    cudaMalloc(&d_fs, (N+1) * sizeof(float));

    /// initialise host-side data
    for (int i=0; i<N; ++i) {
        xs[i] = (i + 0.5) * dx;
        us[i] = initialProfile(xs[i]);
    }

    /// copy host-side data to device
    cudaMemcpy(d_us, us,     N * sizeof(float), cudaMemcpyHostToDevice);
    cudaMemcpy(d_fs, fs, (N+1) * sizeof(float), cudaMemcpyHostToDevice);

    /// calculate blocks / grids, and output that information
    dim3 blockSize(128, 1, 1);
    dim3 gridSize((N + blockSize.x - 1)/blockSize.x, 1, 1);

    printf("blockSize = (%d, %d, %d)\n", blockSize.x,
            blockSize.y, blockSize.z);
    printf("gridSize = (%d, %d, %d)\n", gridSize.x,
            gridSize.y, gridSize.z);

    /// final set-up for running kernel
    float a = 1.0;
    float CFL = 0.5;
    float dt = CFL * dx / a;
    float dtondx = dt / dx;
    int nsteps = 50;

    /// iterate in time, launching flux and value kernels
    for (int n=0; n<nsteps; ++n) {
        calcFluxes<<<gridSize, blockSize>>>(d_us, d_fs, a, N);
        calcValues<<<gridSize, blockSize>>>(d_us, d_fs, dtondx, N);
    }

    /// blocking call to wait until all launched kernels are completed
    cudaError_t err = cudaDeviceSynchronize();

    /// check if we got any errors
    if (err != cudaSuccess) {
        printf("%s\n", cudaGetErrorString(err));
    }

    /// copy device-side data to host
    cudaMemcpy(us, d_us, N * sizeof(float), cudaMemcpyDeviceToHost);

    /// save solution
    FILE *fp = fopen("us_numerical.dat", "w");
    for (int i=0; i<N; ++i) {
        fprintf(fp, "%.8e %.8e\n", xs[i], us[i]);
    }
    fclose(fp);

    /// calculate and save analytical solution
    fp = fopen("us_analytical.dat", "w");
    for (int i=0; i<N; ++i) {
        fprintf(fp, "%.8e %.8e\n", xs[i],
                initialProfile(xs[i] - a * nsteps * dt));
    }
    fclose(fp);

    /// deallocate device-side data
    cudaFree(d_us);
    cudaFree(d_fs);

    /// deallocate host-side data
    free(xs);
    free(us);
    free(fs);
}

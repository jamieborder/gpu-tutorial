#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Sep 24 18:55:24 2020

@author: reid
"""
# PyCUDA initialiazation
import pycuda.autoinit
import pycuda.driver as div
from pycuda import gpuarray
from pycuda.compiler import SourceModule
import numpy as np

from numpy import zeros, array, linspace

# Some helpful functions
def analytical(test_fun, a, L, dt, nsteps):
    xs = linspace(0.0, L, 500)
    t = dt*nsteps
    us = []
    for x in xs:
        us.append(test_fun(x - a*t))
    return xs, array(us)

def write_data_to_file(xs, us, fname):
    f = open(fname, 'w')
    f.write("# x      u\n")
    for x, u in zip(xs, us):
        f.write("%20.12e %20.12e\n" % (x, u))
    f.close()
    return

def iDivUp(a,b):
    return a//b + 1

# test_func
def test_a(x):
        if x >= 2.0 and x <= 3.0:
            return 1
        else:
            return 0.0

# Kernel definition
ker = SourceModule("""
__global__ void fs_cal(float *fs, float *us, float scalar,int N)
  {
  
  int i = blockIdx.x * blockDim.x + threadIdx.x;
  if (i < N) 
  {
  fs[0] = 0.0;
  fs[i] = scalar * us[i-1];
  }
  return;
  }
""")

mod = SourceModule("""
__global__ void us_cal(float *us, float *fs, float scalar,int N)
  {
  
  int i = blockIdx.x * blockDim.x + threadIdx.x;
  if (i < N) 
  {
  us[i] = us[i] - (scalar)*(fs[i+1] - fs[i]);
  }
  return;
  }
""")

# Variables definition
dt = np.float32(0.01)
nsteps = 10
L = np.float32(5.0)
a = np.float32(1.0)
ncells = 100
# ncells = np.int32(100)
    
dx = np.float32(L/ncells)
m = np.float32(dt/dx)
h_xs = np.zeros(ncells,dtype=np.float32) 
h_us = np.zeros(ncells,dtype=np.float32)
h_fs = np.zeros(ncells+1,dtype=np.float32)
'''.astype(np.float32)'''

d_us = np.zeros_like(h_us)
d_fs = np.zeros_like(h_us)
    
for i in range(ncells):
    h_xs[i] = (i+0.5)*dx
    h_us[i] = test_a(h_xs[i])
fscal = ker.get_function("fs_cal")
uscal = mod.get_function("us_cal")       

d_us = gpuarray.to_gpu(h_us)
d_fs = gpuarray.to_gpu(h_fs)

for n in range(nsteps): 
    fscal(d_fs, d_us, a, np.int32(ncells), block = (256,1,1), grid = (iDivUp(ncells,256),1,1) )
    uscal(d_us, d_fs, m, np.int32(ncells), block = (256,1,1), grid = (iDivUp(ncells,256),1,1))       
    # h_fs = d_fs.get()
    # print(h_fs)
        
h_us = d_us.get()
h_fs = d_fs.get()


(xa, ua) = analytical(test_a, a, L, dt, nsteps)
write_data_to_file(xa, ua, "analytical.dat")
write_data_to_file(h_xs,h_us, "numerical.dat")











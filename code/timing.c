#include <stdio.h>
#include <time.h>

int main()
{
    clock_t start = clock(), diff;
    int val[10000];
    for (int i=0; i<1000000; ++i) {
        for (int j=0; j<10000; ++j) {
            val[j] += i;
        }
    }
    diff = clock() - start;

    int msec = diff * 1000 / CLOCKS_PER_SEC;
    printf("Time taken %d seconds %d milliseconds\n", msec/1000, msec%1000);
    printf("val = %d\n", val[10]);
}

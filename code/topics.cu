===================================================================

/// function types ...

// host function - called from host, runs on host
// default if no function type identifier given
// doesn't need 'void' return type
__host__ int hostFunction(arguments ...);
// same as
int hostFunction(arguments ...);

// cuda kernel - called from host, runs on device
// must have 'void' return type
__global__ void kernel(arguments ...);

// device function - called from device, runs on device
__device__ float deviceFunction(arguments ...);

===================================================================

/// memory ...

//register
// thread specific memory; closest
// declared from device
int var;

//local
// thread specific memory if register is full; stored with global
// declared from device
int var;

//shared
// block specific memory; accessible by all threads in a block; faster than global
// declared from device; need to pass size from host if dynamic
__shared__ int var;

//global
// device memory; accessible by all threads on a device
// declared from host using cudaMalloc(..)

//host
// on host; not accessible by device

//constant, texture, ...

===================================================================

/// stuff called from host ...

// declare a pointer to a piece of data (no address yet)
float *data;

// allocate a number of bytes at a certain address, returning that address
cudaMalloc((void **)&data, numberOfBytes);

// free data at a certain address
cudaFree(data);

// copy data (a number of bytes) from one address to another
// need to specify whether a host->device or device->host copy
// is a blocking call (synchronous)
// copy from host to device
cudaMemcpy(dest_address, src_address, numberOfBytes, cudaMemcpyHostToDevice);
// copy from device to host
cudaMemcpy(dest_address, src_address, numberOfBytes, cudaMemcpyDeviceToHost);

// asynchronous copy (launches but doesn't block CPU)
cudaMemcpyAsync( ... same args ... );

// desired dimension of block (i.e. number of threads in x, y, z)
dim3 blockDim3(x, y, z);
// desired dimension of grid (i.e. number of blocks in x, y, z)
dim3 gridDim3(x, y, z);

// launch kernel with desired dimensions of grid and blocks, and 
//  appropriate arguments
// kernel is launched and _not_ waited on, code continues to be run below it
kernel<<< gridDim3, blockDim3 >>>(arguments ...);

// CPU blocking call -- all device code launched before this must finish
//  before code continues after this call
cudaDeviceSynchronize();

// error type which all cuda API functions return
cudaError_t err = ..

// get last cuda error
cudaError_t = cudaGetLastError();

// get string representation of cudaError_t
cudaGetErrorString(err);

===================================================================

/// stuff called from device ...

// x, y, z index for this thread
threadIdx.x, threadIdx.y, threadIdx.z

// x, y, z index for this block
blockIdx.x, blockIdx.y, blockIdx.z

// dimensions of block in each axis (number of threads in each axis)
blockDim.x, blockDim.y, blockDim.z

// dimensions of grid in each axis (number of blocks in each axis)
gridDim.x, gridDim.y, gridDim.z

// synchronizes all threads in a block
_syncthreads();
// (all threads in a warp are already synchronized)

// variable stored in shared memory accessible to all threads in a block
__shared__ int variable;

===================================================================

/// more stuff to discuss, depending how we are going...
// warps
// thread divergence
// memory coalescing
// pitch memory alignment
// prefetching
// memory bound vs compute bound
// streams
// shared memory bank conflicts
// atomic operations

===================================================================

/// first GPU Hackathon agenda ...
Day 1: 25th August 2020  ( 11:30 am – 2:30 pm Aust Eastern Standard Time )

    Welcome (Moderator): (11:30 am – 11:45 am)
    Connecting to Hackathon cluster (11:45 am -12:15 pm)
    Introduction to GPU Programming with OpenACC (12:15 pm - 2:30 pm)
        Introduction to GPU programming (15 min)
            What is a GPU and Why Should You care?
            What is GPU Programming?
            Available Libraries, Programming Models, Platforms
        Introduction to OpenACC (45 min)
            What is OpenACC and Why Should You Care?
            Profile-driven Development
            First Steps with OpenACC
            Lab 1
        OpenACC Data Management (45 min)
            CPU and GPU Memories
            CUDA Unified (Managed) Memory
            OpenACC Data Management
            Lab 2
    Q&A

Day 2: 26th August 2020  ( 11:30 am – 2:30 pm Aust Eastern Standard Time )

    Gangs, Workers, and Vectors Demystified (45 min)
        GPU Profiles
        Loop Optimizations
        Lab
    Mini-application challenge
        Overview of the mini-application (15 min)
        Review steps to acceleration (5 min)
        Application challenge (offline, support through slack)

Day 3: 27th August 2020  ( 11:30 am – 2:30 pm Aust Eastern Standard Time )

    Mini-application Solution Walk-through (11:30 am -11:45 am)
    Introduction to NVIDIA ® Nsight™ Tools (11:45 am -12:15 pm)
        Overview of Nsight Tools
        How to profile a serial application with NVIDIA Tools Extension (NVTX) 
        Overview of optimization cycle with Nsight Systems
    Profiling mini-application (12:15 pm -2:00 pm)
        Profile a sequential weather modeling application (integrated with NVTX APIs) with NVIDIA Nsight Systems to capture and trace CPU events and time ranges
        Understand how to use NVIDIA Nsight Systems profiler’s report to detect hotspots and apply OpenACC compute constructs to the serial application to parallelise it on the GPU
        Learn how to use Nsight Systems to identify issues such as underutilized GPU device and unnecessary data movements in the application and to apply optimization strategies steps by steps to expose more parallelism and utilize computer’s CPU and GPU

Wrap-up and announcing winners (2:00 pm -2:30 pm)

===================================================================

/// Second GPU Hackathon agenda ...
Day 1: 7th Oct 2020  ( 11:30 am – 2:30 pm Aust Eastern Standard Time )

    Welcome (Moderator): (11:30 am – 11:45 am)
    Connecting to Hackathon cluster (11:45 am -12:15 pm)
    CUDA 101 (12:15 – 1:15 pm)
    GPU Memory Hierarchy ( 1:15 – 2:30 )
        GPU Memory Hierarchy evolution
        Global memory

Day 2:  8th Oct 2020  ( 11:30 am – 2:30 pm Aust Eastern Standard Time )

    GPU Memory Hierarchy ( 11:30 – 12:30 )
        Shared Memory
    CUDA Thread Programming (12:30 – 2:30)
        GPU Occupancy
         Atomic operation
         Mixed precision

Day 3:  9th Oct 2020  ( 11:30 am – 2:30 pm Aust Eastern Standard Time )

    CUDA Thread Programming (11:30 – 12:30)
        Warp Divergence and Warp Synchronous programming
    Debugging and Profiling(12:30 – 2:30)
        CUDA Sanitizer
        Nsight profiling Compute: Roofline Analysis

===================================================================

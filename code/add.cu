#include <stdio.h>

__global__ void add(int *a, int *b, int *c)
{
    *c = *a + *b;
}

int main()
{
    /// declare host side variables
    int a, b, c;

    /// declare device side variables
    int *d_a, *d_b, *d_c;

    /// get size of data to allocate on GPU
    int size = sizeof(int);

    /// allocate data for device side variables
    cudaMalloc((void **)&d_a, size);
    cudaMalloc((void **)&d_b, size);
    cudaMalloc((void **)&d_c, size);

    /// initialise host data
    a = 2;
    b = 7;

    /// copy host data to device, initialising device data
    cudaMemcpy(d_a, &a, size, cudaMemcpyHostToDevice);
    cudaMemcpy(d_b, &b, size, cudaMemcpyHostToDevice);

    /// launch kernel
    add<<<1,1>>>(d_a, d_b, d_c);

    /// copy device data to host
    cudaMemcpy(&c, d_c, size, cudaMemcpyDeviceToHost);

    /// check output
    printf("c = %d\n", c);

    /// deallocate data on device
    cudaFree(d_a);
    cudaFree(d_b);
    cudaFree(d_c);

    return 0;
}

#include <stdio.h>

__global__ void dotProduct(const float *x, const float *y, float *z,
        const int sMemSize, const int N)
{
    // shared memory for results of multiplication
    extern __shared__ float s_result[];

    // get this threads global id
    int tid = threadIdx.x + blockIdx.x * blockDim.x;

    // if this thread is greater than the array, return; no work to do
    if (tid >= N) return;
    // if this thread is greater than the sMem, return;
    if (threadIdx.x > sMemSize) return;

    // do dot product multiply
    s_result[threadIdx.x] = x[tid] * y[tid];

    // synchronize all threads in this block
    __syncthreads();
    
    // use the first thread in each block to collect all products
    if (threadIdx.x == 0) {
        float sum = 0.0;
        for (int i = 0; i < sMemSize; ++i) {
            sum += s_result[i];
        }
        z[blockIdx.x] = sum;
    }
}

__global__ void reduce(float *z, const int sMemSize)
{
    // shared memory for results of multiplication
    extern __shared__ float s_result[];

    // get this threads local id (assuming only one block launched)
    int tid = threadIdx.x;

    // if this thread is greater than the array, return; no work to do
    if (tid >= sMemSize) return;

    // all threads load data into shared memory
    s_result[tid] = z[tid];

    // synchronize all threads in this block
    __syncthreads();

    // collect all into one value
    if (tid == 0) {
        float sum = 0.0;
        for (int i = 0; i < sMemSize; ++i) {
            sum += s_result[i];
        }
        z[0] = sum;
    }
}

int main()
{
    /// declare size of arrays in problem
    const int N = 1000;

    /// declare host array variables
    float *x;
    float *y;
    float *z;

    /// allocate host-side data
    x = (float *)malloc(N * sizeof(float));
    y = (float *)malloc(N * sizeof(float));
    z = (float *)malloc(N * sizeof(float));

    /// declare device array variables
    float *d_x;
    float *d_y;
    float *d_z;

    /// allocate device-side data
    cudaMalloc((void **)&d_x, N * sizeof(float));
    cudaMalloc((void **)&d_y, N * sizeof(float));
    cudaMalloc((void **)&d_z, N * sizeof(float));

    /// initialise host-side data
    for (int i=0; i<N; ++i) {
        x[i] = 1.0f * i;
        y[i] = 1.0f * (N-i);
    }

    /// copy host-side data to device
    cudaMemcpy(d_x, x, N * sizeof(float), cudaMemcpyHostToDevice);
    cudaMemcpy(d_y, y, N * sizeof(float), cudaMemcpyHostToDevice);

    /// calculate blocks / grids, and output that information
    dim3 blockSize(128, 1, 1);
    dim3 gridSize((N + blockSize.x - 1)/blockSize.x, 1, 1);

    printf("blockSize = (%d, %d, %d)\n", blockSize.x,
            blockSize.y, blockSize.z);
    printf("gridSize = (%d, %d, %d)\n", gridSize.x,
            gridSize.y, gridSize.z);

    /// calculate amount of shared memory required per block
    int sMemSize = blockSize.x;
    int sMemBytesPerBlock = sMemSize * sizeof(float);

    /// run kernel to calculate products in each block, then collect all
    ///  products within that kernel
    dotProduct<<<gridSize, blockSize, sMemBytesPerBlock>>>(d_x, d_y, d_z,
            sMemSize, N);

    if (blockSize.x > 1024) {
        printf("Algorithm can't handle reduction of more than 1024 values.\n");
    }
    reduce<<<1, blockSize.x, sMemBytesPerBlock>>>(d_z, blockSize.x);

    /// blocking call to wait until all launched kernels are completed
    cudaError_t err = cudaDeviceSynchronize();

    /// check if we got any errors
    if (err != cudaSuccess) {
        printf("%s\n", cudaGetErrorString(err));
    }

    /// copy device-side data to host
    cudaMemcpy(z, d_z, N * sizeof(float), cudaMemcpyDeviceToHost);

    /// check solution
    float hostSum = 0.0;
    for (int i=0; i<N; ++i) {
        hostSum += x[i] * y[i];
    }
    printf("maxError = %f\n", abs(hostSum - z[0]));

    /// deallocate device-side data
    cudaFree(d_x);
    cudaFree(d_y);
    cudaFree(d_z);

    /// deallocate host-side data
    free(x);
    free(y);
    free(z);

    return 0;
}

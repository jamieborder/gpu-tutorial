#include <stdio.h>

int main()
{
    /// declare size of arrays in problem
    const int N = 1000000;

    /// declare host array variables
    float *x;
    float *y;
    float *z;

    /// allocate host-side data
    x = (float *)malloc(N * sizeof(float));
    y = (float *)malloc(N * sizeof(float));
    z = (float *)malloc(N * sizeof(float));

    /// initialise host-side data
    for (int i=0; i<N; ++i) {
        x[i] = 1.0f * i;
        y[i] = 1.0f * (N-i);
    }

    /// run saxpy
    float a = 2.0;
    for (int i = 0; i < N; ++i) {
        z[i] = a * x[i] + y[i];
    }

    /// deallocate host-side data
    free(x);
    free(y);
    free(z);

    return 0;
}

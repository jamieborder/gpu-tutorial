import pycuda.autoinit
import pycuda.driver as div
from pycuda import gpuarray
from pycuda.compiler import SourceModule

import numpy as np

saxpyKernel = SourceModule("""
__global__ void saxpy(const float *x, const float *y, float *z,
        const float a, const int N)
{
    int tid = blockIdx.x;
    if (tid < N) {
        z[tid] = a * x[tid] + y[tid];
    }
}
""")

# Define a reference to the __global__ function
saxpy = saxpyKernel.get_function("saxpy")

# declare size of arrays in problem
N = 100

# Create random vectors on the CPU
x = np.random.randn(N,1).astype(np.float32)
y = np.random.randn(N,1).astype(np.float32)
z = np.zeros_like(x).astype(np.float32)

# Create duplicate arrays on GPU
d_x = gpuarray.to_gpu(x)
d_y = gpuarray.to_gpu(y)
d_z = gpuarray.to_gpu(z)

# calculate blocks / grids, and output that information
nThreads = 1
blockS = (nThreads, 1, 1)
gridS = (N // nThreads + 1, 1, 1)
print("grid size : ", gridS)
print("block size: ", blockS)
print("size of array: ", N)
print("number of threads accessed: ", gridS[0] * blockS[0])

a = np.float32(2.0)

# launch kernel
saxpy(d_x, d_y, d_z, a, np.int32(N), block = blockS, grid = gridS)

div.Context.synchronize()

# copy device-side data to host
z = d_z.get()

# check solution
if np.array_equal(z, a*x + y):
  print("Test passed!")
else :
  print("Error!")
  
# --- Flush context printf buffer
div.Context.synchronize()

-------
RUN JOB
-------

srun --partition=gpu-workshop --pty --gres=gpu:1 --cpus-per-task=1 ./program

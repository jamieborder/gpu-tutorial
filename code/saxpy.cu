#include <stdio.h>

__global__ void saxpy(const float *x, const float *y, float *z,
        const float a, const int N)
{
    // get this threads global id
    int tid = threadIdx.x + blockIdx.x * blockDim.x;
    
    // if a valid thread, access the array
    if (tid < N) {
        // do z = ax + y operation
        z[tid] = a * x[tid] + y[tid];
    }
}

int main()
{
    /// declare size of arrays in problem
    const int N = 1000000;

    /// declare host array variables
    float *x;
    float *y;
    float *z;

    /// allocate host-side data
    x = (float *)malloc(N * sizeof(float));
    y = (float *)malloc(N * sizeof(float));
    z = (float *)malloc(N * sizeof(float));

    /// declare device array variables
    float *d_x;
    float *d_y;
    float *d_z;

    /// allocate device-side data
    cudaMalloc((void **)&d_x, N * sizeof(float));
    cudaMalloc((void **)&d_y, N * sizeof(float));
    cudaMalloc((void **)&d_z, N * sizeof(float));

    /// initialise host-side data
    for (int i=0; i<N; ++i) {
        x[i] = 1.0f * i;
        y[i] = 1.0f * (N-i);
    }

    /// copy host-side data to device
    cudaMemcpy(d_x, x, N * sizeof(float), cudaMemcpyHostToDevice);
    cudaMemcpy(d_y, y, N * sizeof(float), cudaMemcpyHostToDevice);

    /// calculate blocks / grids, and output that information
    dim3 blockSize(128, 1, 1);
    dim3 gridSize((N + blockSize.x - 1)/blockSize.x, 1, 1);

    printf("blockSize = (%d, %d, %d)\n", blockSize.x,
            blockSize.y, blockSize.z);
    printf("gridSize = (%d, %d, %d)\n", gridSize.x,
            gridSize.y, gridSize.z);

    /// run kernel
    float a = 2.0;
    saxpy<<<gridSize, blockSize>>>(d_x, d_y, d_z, a, N);

    /// blocking call to wait until all launched kernels are completed
    cudaError_t err = cudaDeviceSynchronize();

    /// check if we got any errors
    if (err != cudaSuccess) {
        printf("%s\n", cudaGetErrorString(err));
    }

    /// copy device-side data to host
    cudaMemcpy(z, d_z, N * sizeof(float), cudaMemcpyDeviceToHost);

    /// check solution
    float maxError = 0.0;
    for (int i=0; i<N; ++i) {
        maxError = max(maxError, abs(z[i] - (a*x[i] + y[i])));
    }
    printf("maxError = %f\n", maxError);

    /// deallocate device-side data
    cudaFree(d_x);
    cudaFree(d_y);
    cudaFree(d_z);

    /// deallocate host-side data
    free(x);
    free(y);
    free(z);

    return 0;
}
